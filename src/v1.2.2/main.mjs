export function setup(ctx) {
	const id = 'semi-auto-loot';
	const title = 'SEMI Auto Loot';

	const SETTING_GENERAL = ctx.settings.section('General');
	SETTING_GENERAL.add([{
		'type': 'switch',
		'name': `semi-auto-loot-enable`,
		'label': `Enable ${title}`,
		'default': true
	}]);

	ctx.patch(CombatManager, 'onEnemyDeath').after(() => {
		const autoMasterEnabled = SETTING_GENERAL.get(`semi-auto-loot-enable`);
		if (!autoMasterEnabled)
			return;

		const drops = game.combat.loot.drops;
		const count = drops.length;
		if (count > 0) {
			for (let slot = count - 1; slot >= 0; slot--) {
				const drop = drops[slot];
				if (game.bank.addItem(drop.item, drop.quantity, false, true)) {
					game.stats.Combat.add(CombatStats.ItemsLooted, drop.quantity);
					drops.splice(slot, 1);
					game.combat.loot.renderRequired = true;
				}
			}
		}
	});
}
